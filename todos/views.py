from django.shortcuts import render
from django.urls import reverse_lazy
from django.views.generic.list import ListView
from django.views.generic.detail import DetailView
from django.views.generic.edit import CreateView, UpdateView, DeleteView

from todos.models import TodoList, TodoItem

# Create your views here.
class TodoListView(ListView):
    model = TodoList
    template_name = "todos/list.html"

class TodoListDetailView(DetailView):
    model = TodoList
    template_name = "todos/detail.html"

class TodoListCreateView(CreateView):
    model = TodoList
    template_name = "todos/create.html"
    fields = ['name']
    success_url = reverse_lazy("todos_list_view")

# if field is empty, don't redirect, don't submit
# if field is valid, submit and redirect

class TodoListEditView(UpdateView):
    model = TodoList
    template_name = "todos/edit.html"

class TodoListDeleteView(DeleteView):
    model = TodoList
    template_name = "todos/delete.html"


class TodoItemCreateView(CreateView):
    model = TodoItem
    template_name = "items/create.html"
    fields = ['task', 'due_date', 'is_completed']

class TodoItemDetailView(DetailView):
    model = TodoItem
    template_name = "items/detail.html"
    context_object_name = "todo_item"

class TodoItemUpdateView(UpdateView):
    model = TodoItem
    template_name = "items/update.html"
