from django.urls import path

from todos.views import (
    TodoItemCreateView,
    TodoItemUpdateView,
    TodoItemDetailView,

    TodoListCreateView,
    TodoListView, 
    TodoListDetailView,
    TodoListCreateView,
    TodoListEditView,
    TodoListDeleteView,
)

urlpatterns = [
    path("items/create/", TodoItemCreateView.as_view(), name="todo_item_create_view"),
    path("items/<int:pk>", TodoItemDetailView.as_view(), name="todo_item_detail_view"),
    path("items/<int:pk>/edit/", TodoItemUpdateView.as_view(), name="todo_item_update_view"),

    path("", TodoListView.as_view(), name="todos_list_view"),
    path("<int:pk>/", TodoListDetailView.as_view(), name="todo_list_detail_view"),
    path("create/", TodoListCreateView.as_view(), name="todo_list_create_view"),
    path("<int:pk>/edit/", TodoListEditView.as_view(), name="todo_list_edit_view"),
    path("<int:pk>/delete/", TodoListDeleteView.as_view(), name="todo_list_delete_view"),
]